package pl.edu.pwr.wiz.wzorlaboratorium7;

import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static android.content.Intent.ACTION_POWER_CONNECTED;
import static android.content.Intent.ACTION_POWER_DISCONNECTED;

public class MainActivity extends AppCompatActivity {
    private BroadcastReceiver br;
    private LowBatteryBroadcastReceiver lbr;
    private NotificationBroadcastReceiver nbr;
    private BroadcastReceiver JobSchedulerReceiver;

    ChargingTimeService mService;
    boolean mBound = false;

private OnChargeBroadcastReceiver chargeReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* TODO dodać obsługę pola TextView, Button'a i przesłania Intencji do NotificationBroadcastReceiver'a */
        Button msgBnt = findViewById(R.id.show_msg_btn);
        msgBnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText text = findViewById(R.id.notification_text);
                Intent notificationIntent = new Intent("notification_sent");
                notificationIntent.putExtra(NotificationBroadcastReceiver.NOTIFICATION_ID, 1);
                notificationIntent.putExtra(NotificationBroadcastReceiver.NOTIFICATION, text.getText().toString());
                sendBroadcast(notificationIntent);
            }
        });

        Button JbBtn = findViewById(R.id.job_scheduler_btn);
        JbBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //start job scheduler
                if (Build.VERSION.SDK_INT<21)
                {
                    final Snackbar mySnackbar = Snackbar.make(findViewById(R.id.activity_main),
                            "Kup nowy telefon. Min andoid 5", Snackbar.LENGTH_SHORT);
                    mySnackbar.setAction("OK", new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            //do something
                            mySnackbar.dismiss();
                        }
                    });
                    mySnackbar.show();
                    //show message
                } else {
                    JobScheduler mJobScheduler = (JobScheduler)
                            getSystemService( Context.JOB_SCHEDULER_SERVICE );

                    JobInfo.Builder builder = new JobInfo.Builder( 1,
                            new ComponentName( getPackageName(),
                                    JobsShedulerService.class.getName() ) );
                    builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
//builder.setRequiresCharging(true);
                    mJobScheduler.schedule( builder.build() );
                }
            }
        });

        JobSchedulerReceiver = new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                Snackbar mySnackbar = Snackbar.make(findViewById(R.id.activity_main),
                "Przeglądarka jast gotowa do uruchamienia", Snackbar.LENGTH_SHORT);
                mySnackbar.setAction("URUCHOM", new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        //do something
                        String url = "http://www.google.com";
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                    }
                }).setDuration(10000);
                mySnackbar.show();            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();

        /* Rejestrujemy nasłuch na stany baterii
           https://developer.android.com/reference/android/content/Intent.html#ACTION_BATTERY_CHANGED
           https://developer.android.com/reference/android/os/BatteryManager.html
        */
        IntentFilter filter = new IntentFilter();
        if (!mBound) {
            br = new BatteryBroadcastReceiver();


            filter.addAction(Intent.ACTION_BATTERY_CHANGED);
            this.registerReceiver(br, filter);
        }
nbr = new NotificationBroadcastReceiver();
filter = new IntentFilter();
        filter.addAction("notification_sent");
this.registerReceiver(nbr,filter);
chargeReceiver = new OnChargeBroadcastReceiver();
filter = new IntentFilter();
filter.addAction(ACTION_POWER_CONNECTED);
        filter.addAction(ACTION_POWER_DISCONNECTED);
        this.registerReceiver(chargeReceiver,filter);
        this.registerReceiver(JobSchedulerReceiver, new IntentFilter("EVENT_SNACKBAR"));

    }

    @Override
    protected void onPause() {
        /* Wyrejestrowujemy nasłuch na stany baterii */
        if (!mBound) {
            this.unregisterReceiver(br);
        }
        this.unregisterReceiver(nbr);
        this.unregisterReceiver(chargeReceiver);
        super.onPause();
    }

    /* Funkcja uruchamia usługę timera */
    public void startTimer(View view) {
        Intent intent = new Intent(this, TimerService.class);
        startService(intent);
    }

    /* Funkcja zatrzymuje usługę timera */
    public void stopTimer(View view) {
        Intent intent = new Intent(this, TimerService.class);
        stopService(intent);
    }

    public void startChargingTimer(){
        Intent intent = new Intent(this, ChargingTimeService.class);

        getApplicationContext().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        startService(intent);
    }

    public void stopChargingTimer(){

        if (mBound) {
           // unbindService(mConnection);
            mService.stop();

            mBound = false;
        }
        Intent intent = new Intent(this, ChargingTimeService.class);
        stopService(intent);
    }

    public void SetBatteryLevel(float percent){
        if (mBound){
            mService.setBatteryLevel(percent);
        }

    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ChargingTimeService.LocalBinder binder = (ChargingTimeService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };
}
