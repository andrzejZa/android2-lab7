package pl.edu.pwr.wiz.wzorlaboratorium7;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class LowBatteryBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        Toast.makeText(((Activity)context).getApplicationContext(),"Podłącz ładowarkę",Toast.LENGTH_LONG).show();
    }
}
