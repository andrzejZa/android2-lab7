package pl.edu.pwr.wiz.wzorlaboratorium7;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class ChargingTimeService extends IntentService {
    private final IBinder mBinder = new LocalBinder();
    private Integer timer = 0;
    private Timer t;
    private static  ChargingTimeService instance;
    private  float level;
    public static void StartTimer(){
        if (instance == null)
            instance = new ChargingTimeService();


    }
    public ChargingTimeService() {
        super("ChargingTimeService");

    }

    @Override
    public void onDestroy() {
        t.cancel();

        ((NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE)).cancelAll();
        super.onDestroy();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        t = new Timer();
        t.scheduleAtFixedRate(new task(this,pendingIntent),new Date(), 1000);
        /* Nieskonczona petla, dzialamy dopoki ktos nas nie zatrzyma */
//        while(true) {
//            /* Tworzymy PendingIntent, który wywoła naszą aktywność w razie kliknięcia w notyfikację */
//            Intent notificationIntent = new Intent(this, MainActivity.class);
//            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
//
//            /* Budujemy notyfikację */
//            Notification notification = new Notification.Builder(this)
//                    .setContentTitle(getText(R.string.counter_title))
//                    .setContentText(getText(R.string.counter_body) + timer.toString() + "s")
//                    .setSmallIcon(R.drawable.ic_av_timer_black_24dp)
//                    .setContentIntent(pendingIntent)
//                    .build();
//
//            /* Startujemy na pierwszym planie, aby nie dało jej się zamknąć */
//            startForeground(3, notification);
//
//            try {
//                Thread.sleep(5000);
//                timer += 5; // dodajemy 5 sekund
//            } catch (InterruptedException e) {
//                // Restore interrupt status.
//                Thread.currentThread().interrupt();
//            }
//        }

    }


    class task extends TimerTask{
        private  Context context;
        private  PendingIntent pendingIntent;
            public task(Context context, PendingIntent pendingIntent){
                this.context = context;
                this.pendingIntent = pendingIntent;
            }
        @Override
        public void run() {
            /* Budujemy notyfikację */
            Notification notification = new Notification.Builder(context)
                    .setContentTitle("Ladowanie")
                    .setContentText("Pozostalo do naladowania: " + TimeCounter.Get().getStringLeft())
                    .setSmallIcon(R.drawable.ic_av_timer_black_24dp)
                    .setContentIntent(pendingIntent)
                    .build();

            /* Startujemy na pierwszym planie, aby nie dało jej się zamknąć */
            startForeground(3, notification);
        }


    }

    public class LocalBinder extends Binder {
        ChargingTimeService getService() {
            // Return this instance of LocalService so clients can call public methods
            return ChargingTimeService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /** method for clients */
    public void setBatteryLevel( float level) {
       this.level = level;
        TimeCounter.Get().setLevel(level);

    }
    public void stop(){
        t.cancel();
        TimeCounter.Reset();
        stopForeground(true);

    }


}
class TimeCounter{
    private static TimeCounter instance;
    private Date timeStart;
    private Date timeLeft;
    private Float _level;
    private Float startLevel;
    private Long interval;
    public Date getTimeLeft(){
        if (timeLeft != null){
            timeLeft = new Date(timeLeft.getTime() - 1000);
            return timeLeft;
        }
            return null;
    }

    public String getStringLeft(){
        Date dateLeft = getTimeLeft();
        if (dateLeft == null)
            return "obliczam...";
        long diff = dateLeft.getTime() - new Date().getTime();
        Date DiffEnd = new Date(diff);
        if (DiffEnd.getHours() !=0)
            return DiffEnd.getHours()+":"+DiffEnd.getMinutes()+":"+DiffEnd.getSeconds();
        else
            return  DiffEnd.getMinutes()+":"+DiffEnd.getSeconds();
    }
    private TimeCounter(){

    }
    public void setLevel(float level){
    if (_level != null && _level != level){
        _level = level;
        interval = new Date().getTime() - timeStart.getTime();
        float left = (100-_level)*interval/(_level-startLevel);
        timeLeft = new Date(new Date().getTime()+ (long)left);
        return;
    }
        if (startLevel == null) {
            startLevel = level;
            timeStart = new Date();
      }

        _level = level;

    }
    public static void Reset(){
        instance = null;
    }
    public static TimeCounter Get(){
        if (instance == null)
            instance = new TimeCounter();
        return instance;
    }
}
