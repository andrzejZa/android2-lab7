package pl.edu.pwr.wiz.wzorlaboratorium7;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationBroadcastReceiver extends BroadcastReceiver {
    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION = "notification";
    @Override
    public void onReceive(Context context, Intent intent) {
        /* TODO Pobierać dane z intencji i wyświetlać notyfikację systemową z przesłanym tekstem */


            NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

           // Notification notification = intent.getParcelableExtra(NOTIFICATION);
            int id = intent.getIntExtra(NOTIFICATION_ID, 0);
            String text = intent.getStringExtra(NOTIFICATION);
        Notification notification = new Notification.Builder(context)
                .setContentTitle("Notification")
                .setContentText(text)
                .setSmallIcon(R.drawable.ic_email)

                .build();
        notificationManager.notify(1,notification);

    }
}