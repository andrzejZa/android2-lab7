package pl.edu.pwr.wiz.wzorlaboratorium7;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class JobsShedulerService extends JobService {

    private Handler mJobHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage( Message msg ) {
            Toast.makeText( getApplicationContext(),
                    "JobService task running", Toast.LENGTH_SHORT )
                    .show();
           // jobFinished( (JobParameters) msg.obj, false );
            return true;
        }

    } );

    @Override
    public boolean onStartJob(JobParameters params) {
        Intent it = new Intent("EVENT_SNACKBAR");
        //LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(it);
        getApplicationContext().sendBroadcast(it);

        mJobHandler.handleMessage(Message.obtain( mJobHandler, 1, params ) );

        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
