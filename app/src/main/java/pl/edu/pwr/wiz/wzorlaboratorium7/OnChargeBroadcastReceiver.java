package pl.edu.pwr.wiz.wzorlaboratorium7;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.view.View;
import android.widget.Toast;

public class OnChargeBroadcastReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if(action.equals(Intent.ACTION_POWER_CONNECTED))
        {
            ((MainActivity)context).startChargingTimer();
            // Do something when power connected
            Toast.makeText(context, "connected", 3000).show();
        }
        else if(action.equals(Intent.ACTION_POWER_DISCONNECTED))
        {
            ((MainActivity)context).stopChargingTimer();
            // Do something when power disconnected
            Toast.makeText(context, "Disconnected", 3000).show();
        }
    }
}
